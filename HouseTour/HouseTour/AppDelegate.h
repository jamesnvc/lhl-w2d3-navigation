//
//  AppDelegate.h
//  HouseTour
//
//  Created by James Cash on 10-10-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

